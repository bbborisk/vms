<?php
/**
 * Calendar view for shifts
 */

if (!isset($_SESSION['user_id'])) {
    header("Location: index.php");
    die();
}

$user_id = $_SESSION['user_id'];

if (isset($_GET["month"])) {
    $month = (int) $_GET["month"];
} else {
    $month = (int) date('n');
}

if (isset($_GET["year"])) {
    $year = (int) $_GET["year"];
} else {
    $year = (int) date('Y');
}

// Start date of current month and start date of next month so we loop through all dates in between
$first_day_of_selected_month = mktime(0, 0, 0, $month, 1, $year);
$first_day_of_previous_month = strtotime('-1 month', $first_day_of_selected_month);
$first_day_of_next_month = strtotime('+1 month', $first_day_of_selected_month);
$today = mktime(0, 0, 0, date('n'), date('j'), date('Y'));
$five_days_after_today = strtotime('+5 days', $today);
$previous_month = date('n', $first_day_of_previous_month);
$next_month = date('n', $first_day_of_next_month);
$previous_months_year = date('Y', $first_day_of_previous_month);
$next_months_year = date('Y', $first_day_of_next_month);

$month_name = $i18n['month.' . $month];

if (isset($_POST["submit"])) {
    $new_shifts = [];
    if (!empty($_POST['shifts'])) {
        $shift_ids = array_keys($_POST['shifts']);
        foreach ($shift_ids as $shift_id) {
            $new_shifts[$shift_id] = true;
        }
    }

    $old_shifts = [];
    $select_shifts = "SELECT user_shifts.shift_id FROM user_shifts JOIN shifts ON user_shifts.shift_id = shifts.id WHERE user_shifts.user_id = {$user_id} AND shifts.date < '" . date('Y-m-d', $first_day_of_next_month) . "' AND shifts.date >= '" . date('Y-m-d', $first_day_of_selected_month) . "'";
    if (!$result = pg_query($select_shifts)) {
        die("Error executing query." . pg_last_error());
    } else {
        while ($user_shift = pg_fetch_assoc($result)) {
            $old_shifts[$user_shift["shift_id"]] = true;
        }
    }

    $affected_shifts = array_merge(array_keys($new_shifts), array_keys($old_shifts));

    $shifts_to_remove = [];
    $shifts_to_add = [];
    foreach ($affected_shifts as $shift_id) {
        if (isset($new_shifts[$shift_id]) && $new_shifts[$shift_id] && !isset($old_shifts[$shift_id])) {
            array_push($shifts_to_add, $shift_id);
        } elseif (isset($old_shifts[$shift_id]) && $old_shifts[$shift_id] && !isset($new_shifts[$shift_id])) {
            array_push($shifts_to_remove, $shift_id);
        }
    }

    if (!empty($shifts_to_add)) {
        $insert_shifts = "INSERT INTO user_shifts(shift_id, user_id)
                          VALUES ";
        $number_of_shifts_to_add = count($shifts_to_add);
        for ($i = 0; $i < $number_of_shifts_to_add; $i++) {
            $shift_id = $shifts_to_add[$i];
            $insert_shifts .= "('{$shift_id}', '{$user_id}')";
            if ($i < $number_of_shifts_to_add - 1) {
                $insert_shifts .= ", ";
            }
        }
        if (!$result = pg_query($insert_shifts)) {
            die("Error executing query." . pg_last_error());
        }
    }
    if (!empty($shifts_to_remove)) {
        $delete_shifts = "DELETE FROM user_shifts
                          WHERE user_id = '{$user_id}' AND shift_id IN (" . implode(', ', $shifts_to_remove) . ")";
        if (!$result = pg_query($delete_shifts)) {
            die("Error executing query." . pg_last_error());
        }
    }
    logAdd("[change_shifts] User '{$_SESSION['username']}' changed his shifts.");
}

?>
<div class="container whitebg-full">
    <a style="float: left;" type="button" class="btn btn-default btn-sm" href="?content=calendar&month=<?php echo $previous_month; ?>&year=<?php echo $previous_months_year; ?>"><span class="glyphicon glyphicon-arrow-left"></span> <?php echo $i18n['calendar.previous_month'] ?></a>
    <a style="float:right;" type="button" class="btn btn-default btn-sm" href="?content=calendar&month=<?php echo $next_month; ?>&year=<?php echo $next_months_year; ?>"><?php echo $i18n['calendar.next_month'] ?> <span class="glyphicon glyphicon-arrow-right"></span></a>
    <form role="form" action="" method="POST">
        <table class="table table-bordered table-unfloat">
            <caption>
                <h2><?php echo $month_name . " " . $year; ?></h2>
            </caption>
            <thead>
                <tr>

<?php
foreach (range(1, 7) as $day_of_week) {
    echo '          <th>';
    echo '              <b>' . $i18n['day_of_week.' . $day_of_week] . '</b>';
    echo '          </th>';
}
?>
                </tr>
            </thead>
            <tbody>
                <tr>
<?php
// Fill empty days until the 1st of the month, if any
for ($j = 0; $j < (int) (date('N', $first_day_of_selected_month)) - 1; $j++) {
    echo '          <td>&nbsp;</td>';
}
// Print all the days for selected month
// Day of the week starts from the last value of j, last not printed day of the week
for ($day_of_week = $j, $current_date = $first_day_of_selected_month;
     $current_date < $first_day_of_next_month;
     $day_of_week++, $current_date = strtotime('+1 day', $current_date)) {
    $current_date_year = (int) date('Y', $current_date);
    $current_date_month = (int) date('n', $current_date);
    $current_date_day = (int) date('j', $current_date);
    // From Sunday to Monday, new row
    if ($day_of_week % 7 == 0) {
        echo '  </tr>';
        echo '  <tr>';
    }
    // Print day
    $critical_coefficient = criticalShift($current_date);
    $day_class = "";
    if ($current_date == $today && $critical_coefficient < 1) {
        $day_class = 'class="danger"';
    }
    if ($current_date > $today && $current_date <= $five_days_after_today) {
        if ($critical_coefficient < 0.5) {
            $day_class = 'class="danger"';
        } elseif ($critical_coefficient < 1) {
            $day_class = 'class="warning"';
        }
    }
    echo '          <td ' . $day_class . '>';
    echo '              <a href=index.php?content=day&year=' . $current_date_year . '&month=' . $current_date_month . '&day=' . $current_date_day . '>' . $current_date_day . '</a>';
    echo getShifts($current_date);
    echo '          </td>';
}
// Fill empty days after last date in selected month until the end of the week
if ($day_of_week % 7 != 0) {
    for ($j = $day_of_week % 7; $j < 7; $j++) {
        echo '      <td>&nbsp;</td>';
    }
}
?>
                </tr>
            <tbody>
        </table>
        <input class="btn btn-lg btn-primary btn-block" type="submit" id="submit" name="submit" value="<?php echo $i18n['calendar.apply_changes'] ?>"/>
    </form>
</div>
