<?php
/**
 * Content loader
 */

$content = 'welcome';
if (isset($_GET["content"])) {
    $content = $_GET["content"];
}


// User is not logged in ore requesting to reset password, show login page
if (!isset($_SESSION["username"])
    && !in_array($content, ['users_password_reset', 'users_lost_password'])
) {
?>
<div class="container">
<?php
include 'users_login.php';
?>
</div>
<?php
die();
}

$show_navigation = false;
$include_content = false;
switch ($content) {
case 'users_lost_password':
case 'users_password_reset':
case 'users_logout':
    $include_content = true;
    break;
case 'users_index':
case 'users_new':
case 'users_edit':
case 'events_index':
case 'events_new':
case 'events_show':
case 'events_edit':
case 'events_destroy':
case 'user_shifts_new':
case 'user_shifts_destroy':
case 'user_shifts_sum_by_event':
case 'shifts_index':
case 'shifts_new':
case 'shifts_edit':
case 'shifts_destroy':
case 'notices_index':
case 'notices_destroy':
case 'calendar':
case 'day':
case 'log':
case 'welcome':
    $show_navigation = true;
    $include_content = true;
    break;
default:
    header("Location: index.php");
    die();
    break;
}
if ($show_navigation) {
    include 'navigation.php';
}
if ($include_content) {
    include $content . '.php';
}
