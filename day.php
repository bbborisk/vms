<?php
/**
 * Daily view for shifts
 */

if (!isset($_SESSION['user_id'])) {
    header("Location: index.php");
    die();
}

$user_id = $_SESSION['user_id'];

if (isset($_GET["day"])) {
    $day = (int) $_GET["day"];
} else {
    $day = (int) date('j');
}

if (isset($_GET["month"])) {
    $month = (int) $_GET["month"];
} else {
    $month = (int) date('n');
}

if (isset($_GET["year"])) {
    $year = (int) $_GET["year"];
} else {
    $year = (int) date('Y');
}

$selected_date = mktime(0, 0, 0, $month, $day, $year);
$selected_date_string = date('Y-m-d', $selected_date);
$previous_date = strtotime('-1 day', $selected_date);
$next_date = strtotime('+1 day', $selected_date);
$previous_dates_day = date('j', $previous_date);
$next_dates_day = date('j', $next_date);
$previous_dates_month = date('n', $previous_date);
$next_dates_month = date('n', $next_date);
$previous_dates_year = date('Y', $previous_date);
$next_dates_year = date('Y', $next_date);

$month_name = $i18n['month.' . $month];
$day_name = $i18n['day_of_week.' . date('N', $selected_date)];
?>
<div class="container whitebg-full">
    <a style="float: left;" type="button" class="btn btn-default btn-sm" href="?content=day&year=<?php echo $previous_dates_year; ?>&month=<?php echo $previous_dates_month; ?>&day=<?php echo $previous_dates_day; ?>"><span class="glyphicon glyphicon-arrow-left"> <?php echo $i18n['day.previous_day'] ?></span></a>
    <a style="float: right;" type="button" class="btn btn-default btn-sm" href="?content=day&year=<?php echo $next_dates_year; ?>&month=<?php echo $next_dates_month; ?>&day=<?php echo $next_dates_day; ?>"><?php echo $i18n['day.next_day'] ?> <span class="glyphicon glyphicon-arrow-right"></span></a>
    <div class="text-center">
        <h2><?php echo $day_name . ', ' . $day . ' ' . $month_name . ' ' . $year; ?></h2>
    </div>
<?php
$select_shifts = "SELECT shifts.*, events.name AS event_name FROM shifts JOIN events ON shifts.event_id = events.id WHERE shifts.date = '{$selected_date_string}' ORDER BY event_name, shifts.start_time";
if (!$result = pg_query($select_shifts)) {
    die("Error executing query." . pg_last_error());
} else {
    if (pg_num_rows($result) == 0) {
?>
    <div class="alert alert-info">
        <?php echo $i18n['day.no_shifts_for_day'] ?>
    </div>
<?php
    } else {
        $current_event_id = null;
        while ($shift = pg_fetch_assoc($result)) {
            $shift_id = $shift['id'];
            $event_id = $shift['event_id'];
            $event_name = $shift['event_name'];
            if ($event_id != $current_event_id) {
                $current_event_id = $event_id;
?>
    <h1><?php echo $event_name ?></h1>
<?php
            }
?>
    <table class="table table-striped table-bordered">
        <caption>
            <h4><?php echo strftime('%H:%M', strtotime($shift['start_time'])) . ' &ndash; ' . strftime('%H:%M', strtotime($shift['end_time'])) ?></h4>
<?php
            if ($_SESSION['user_type'] == 1) {
?>
            <a class="btn btn-default btn-xs" href="index.php?content=shifts_edit&shift_id=<?php echo $shift_id ?>&redirect_back_url=<?php echo urlencode($_SERVER['REQUEST_URI']) ?>"><span class="glyphicon glyphicon-edit"></span> <?php echo $i18n['shifts.edit_shift'] ?></a>
            <a class="btn btn-default btn-xs" href="index.php?content=shifts_destroy&shift_id=<?php echo $shift_id ?>&redirect_back_url=<?php echo urlencode($_SERVER['REQUEST_URI']) ?>"><span class="glyphicon glyphicon-trash"></span> <?php echo $i18n['shifts.remove_shift'] ?></a>
<?php
            }
?>
        </caption>
        <tbody>
<?php
            $select_user_shifts = "SELECT id, user_id
                                   FROM user_shifts
                                   WHERE shift_id = '{$shift_id}'";
            if (!$result_user_shifts = pg_query($select_user_shifts)) {
                die("Error executing query." . pg_last_error());
            } else {
                if (pg_num_rows($result_user_shifts) == 0) {
?>
            <tr>
                <td>
                    <span><?php echo $i18n['day.nobody_applied_for_shift'] ?></span>
                </td>
            </tr>
<?php
                } else {
                    while ($user_shift = pg_fetch_assoc($result_user_shifts)) {
?>
            <tr>
                <td>
<?php
                        echo userListItem($user_shift['user_id']);
                        if ($_SESSION['user_type'] < 3) {
?>
                    <a style="float: right;" type="button" class="btn btn-default btn-xs" href="index.php?content=user_shifts_destroy&user_shift_id=<?php echo $user_shift['id'] ?>&redirect_back_url=<?php echo urlencode($_SERVER['REQUEST_URI']) ?>"><span class="glyphicon glyphicon-trash"></span> <?php echo $i18n['remove'] ?></a>
<?php
                        }
?>
                </td>
            </tr>
<?php
                    }
                }
                if ($_SESSION['user_type'] < 3) {
?>
            <tr>
                <td>
                    <a type="button" class="btn btn-default btn-xs" href="index.php?content=user_shifts_new&shift_id=<?php echo $shift_id ?>"><span class="glyphicon glyphicon-plus"></span> <?php echo $i18n['add'] ?></a>
                </td>
            </tr>
<?php
                }
            }
?>
        </tbody>
    </table>
<?php
        }
    }
}
?>
</div>
