<?php
/**
 * Initial connection to the batabase
 */

// get connection settings and other secret settings
require 'secrets.inc';
$hostname = $_secrets['hostname'];
$username = $_secrets['username'];
$password = $_secrets['password'];
$database = $_secrets['database'];

// connect to the server
$connection = pg_connect("host={$hostname} port=5432 dbname={$database} user={$username} password={$password}");
if (!$connection) {
    die("Error establishing a connection with the batabase server.");
}

