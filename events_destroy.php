<?php
/**
 * Remove event page
 */

if (!isset($_SESSION['user_id']) || !(isset($_SESSION['user_type']) && $_SESSION['user_type'] == 1)) {
    header("Location: index.php");
    die();
}

$event_id = clean($_GET['id']);

// Dependent destroy
// TODO: Maybe ensure this by DB design
// by adding cascade destroy for foreign keys
$delete_event_user_shifts = "
    DELETE FROM user_shifts
    WHERE shift_id IN (
        SELECT id
        FROM shifts
        WHERE event_id = {$event_id}
    )
";
if (!$query_result = pg_query($delete_event_user_shifts)) {
    die("Error executing query. " . pg_last_error());
}

$delete_event_shifts = "
    DELETE FROM shifts
    WHERE event_id = {$event_id}
";
if (!$query_result = pg_query($delete_event_shifts)) {
    die("Error executing query. " . pg_last_error());
}

$delete_event = "
    DELETE FROM events
    WHERE id = {$event_id}
";
if (!$query_result = pg_query($delete_event)) {
    die("Error executing query. " . pg_last_error());
} else {
    logAdd("[remove_event] User '{$_SESSION['username']}' deleted event (id = {$event_id}).");
    header("Location: index.php?content=events_index");
    die();
}
