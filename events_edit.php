<?php
/**
 * Edit event page
 */

if (isset($_GET["id"])) {
    $id = $_GET["id"];
}

if ($id && (!isset($_SESSION['user_id']) || !(isset($_SESSION['user_type']) && $_SESSION['user_type'] == 1))) {
    header("Location: index.php");
    die();
}

if (isset($_POST["submit"])) {
    $start_date = clean($_POST["start_date"]);
    $end_date = clean($_POST["end_date"]);
    $name = clean($_POST["name"]);
    $description = clean($_POST["description"]);
    $update_event = "
        UPDATE events
        SET
            start_date = '{$start_date}',
            end_date = '{$end_date}',
            name = '{$name}',
            description = '{$description}'
        WHERE id = {$id}";
    if (!$result_event = pg_query($update_event)) {
        die("Error executing query." . pg_last_error());
    } else {
        logAdd("[update_event] User '{$_SESSION['username']}' changed event (id = {$id}).");
        header("Location: index.php?content=events_index");
        die();
    }
}
$select_event = "
    SELECT *
    FROM events
    WHERE id = {$id}";
if (!$result = pg_query($select_event)) {
    die("Error executing query." . pg_last_error());
} else {
    if (pg_num_rows($result) != 0) {
        $event = pg_fetch_assoc($result);
    } else {
        header("Location: index.php?content=events_index");
        die();
    }
}
?>
<div class="container whitebg">
    <form class="form-other" role="form" method="POST" action="">
        <h2><?php echo $i18n['events.edit']; ?></h2>
        <br/>
        <input class="form-control form-control-top" type="text" required="" value="<?php echo $event['name']; ?>" id="name" name="name"/>
        <input class="form-control form-control-top form-control-bottom" type="text" required="" value="<?php echo $event['description']; ?>" id="description" name="description"/>
        <input class="form-control form-control-top form-control-bottom picker" type="text" required="" value="<?php echo $event['start_date']; ?>" id="start_date" name="start_date"/>
        <input class="form-control form-control-bottom picker" type="text" required="" value="<?php echo $event['end_date']; ?>" id="end_date" name="end_date"/>
        <br/>
        <input class="btn btn-lg btn-primary btn-block" type="submit" id="submit" name="submit" value="<?php echo $i18n['save']; ?>"/>
        <br/>
        <div class="alert alert-warning">
            <?php echo $i18n['events.edit']; ?>
        </div>
    </form>
    <script type="text/javascript">
        $("#start_date").datepicker( { format: "yyyy-mm-dd", weekStart: 1 } );
        $("#end_date").datepicker( { format: "yyyy-mm-dd", weekStart: 1 } );
    </script>
</div>
