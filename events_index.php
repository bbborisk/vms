<?php
/**
 * Events page
 */

if (!isset($_SESSION['user_id']) || $_SESSION['user_type'] > 1) {
    header("Location: index.php");
    die();
}

$select_event = "SELECT * FROM events";
if (!$result = pg_query($select_event)) {
    die("Error executing query." . pg_last_error());
} else {
?>
<div class="whitebg-mid">
    <table class='table table-striped table-bordered'>
        <caption>
            <h3><?php echo $i18n['events.title'] ?></h3>
        </caption>
        <thead>
            <tr>
                <th>
                    <b><?php echo $i18n['events.number_sign'] ?></b>
                </th>
                <th>
                    <b><?php echo $i18n['events.name'] ?></b>
                </th>
                <th>
                    <b><?php echo $i18n['events.start_date'] ?></b>
                </th>
                <th>
                    <b><?php echo $i18n['events.end_date'] ?></b>
                </th>
                <th>
                    <b><?php echo $i18n['events.description'] ?></b>
                </th>
                <th>
                    <b><?php echo $i18n['events.action'] ?></b>
                </th>
            </tr>
        </thead>
        <tbody>
<?php
    if (pg_num_rows($result) == 0) {
?>
            <tr>
                <td class="text-center" colspan="6"><?php echo $i18n['events.empty'] ?></td>
            </tr>
<?php
    } else {
        while ($event = pg_fetch_assoc($result)) {
?>
            <tr>
                <td><?php echo $event["id"] ?></td>
                <td><?php echo $event["name"] ?></td>
                <td><?php echo $event["start_date"] ?></td>
                <td><?php echo $event["end_date"] ?></td>
                <td><?php echo $event["description"] ?></td>
                <td>
                    <a class="btn btn-default btn-xs" href="index.php?content=events_destroy&id=<?php echo $event['id']; ?>"><span class="glyphicon glyphicon-trash"> <?php echo $i18n['delete'] ?></span></a>
                    <a class="btn btn-default btn-xs" href="index.php?content=events_edit&id=<?php echo $event['id']; ?>"><span class="glyphicon glyphicon-edit"> <?php echo $i18n['events.edit_info'] ?></span></a>
                    <a class="btn btn-default btn-xs" href="index.php?content=shifts_index&event_id=<?php echo $event['id']; ?>"><span class="glyphicon glyphicon-edit"> <?php echo $i18n['events.show_shifts'] ?></span></a>
                </td>
            </tr>
<?php
        }
    }
?>
            <tr>
                <td class="text-left" colspan="6">
                    <a class="btn btn-default btn-xs" href="index.php?content=events_new"><span class="glyphicon glyphicon-plus"> <?php echo $i18n['events.add_new'] ?></span></a>
                </td>
            </tr>
        </tbody>
    </table>
<?php
}
?>
</div>
