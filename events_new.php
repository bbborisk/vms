<?php
/**
 * Add new event page
 */

if (!isset($_SESSION['user_id']) || $_SESSION['user_type'] > 1) {
    header("Location: index.php");
    die();
}

$message = false;
if (isset($_POST["submit"])) {
    $name = clean($_POST["name"]);
    $start_date = clean($_POST["start_date"]);
    $end_date = clean($_POST["end_date"]);
    $description = clean($_POST["description"]);
    $shift_hours = clean($_POST["shift_hours"]);
    $shift_hours_by_shift = explode(',', $shift_hours);
    $people_needed = clean($_POST["people_needed"]);
    $people_needed_by_shift = explode(',', $people_needed);
    $number_of_shifts_by_shifts = count($shift_hours_by_shift);
    $number_of_shifts_by_people_needed = count($people_needed_by_shift);
    if ($number_of_shifts_by_shifts != $number_of_shifts_by_people_needed) {
        $message = $i18n['events.shifts_and_people_needed_count_mismatch'];
    } else {
        $shifts = array();
        for ($i = 0; $i < $number_of_shifts_by_shifts; $i++) {
            $shift = array();
            list($shift['start_time'], $shift['end_time']) = explode('-', $shift_hours_by_shift[$i]);
            $shift['people_needed'] = $people_needed_by_shift[$i];
            $shifts[$i] = $shift;
        }
        $insert_event = "
            INSERT INTO events(name, description, start_date, end_date)
            VALUES ('{$name}', '{$description}', '{$start_date}', '{$end_date}')";
        if (!$result = pg_query($insert_event)) {
            die("Error executing query." . pg_last_error());
        } else {
            if (!$result_event = pg_query('SELECT LASTVAL() AS id')) {
                die("Error executing query." . pg_last_error());
            } else {
                $last_inserted_id = pg_fetch_assoc($result_event);
                $event_id = $last_inserted_id['id'];
                $first_day = strtotime($start_date);
                $last_day = strtotime($end_date);
                $insert_shifts = "
                    INSERT INTO shifts(event_id, date, start_time, end_time, people_needed)
                    VALUES ";
                for ($current_day = $first_day; $current_day <= $last_day; $current_day = strtotime('+1 day', $current_day)) {
                    $date = date('Y-m-d', $current_day);
                    $number_of_shifts = count($shifts);
                    for ($i = 0; $i < $number_of_shifts; $i++) {
                        $start_time = $shifts[$i]['start_time'];
                        $end_time = $shifts[$i]['end_time'];
                        $people_needed = $shifts[$i]['people_needed'];
                        $insert_shifts .= "('{$event_id}', '{$date}', '{$start_time}', '{$end_time}', '{$people_needed}')";
                        if (($i < $number_of_shifts - 1) || (strtotime('+1 day', $current_day) <= $last_day)) {
                            $insert_shifts .= ',';
                        }
                    }
                }
                if (!$result_shifts = pg_query($insert_shifts)) {
                    die("Error executing query." . pg_last_error());
                } else {
                    logAdd("[new_event] User '{$_SESSION['username']}' added a new event ('{$name}', id = {$event_id}).");
                    $message = $i18n['events.created_success'];
                }
            }
        }
    }
}
?>
<div class="container whitebg">
    <form class="form-other" role="form" method="POST" action="" onsubmit="return checkNewEventForm()">
        <h2><?php echo $i18n['events.add_new']; ?></h2>
        <br/>
        <input class="form-control form-control-top" type="text" required="" placeholder="<?php echo $i18n['events.name_placeholder']; ?>" id="name" name="name"/>
        <input class="form-control form-control-top form-control-bottom picker" type="text" required="" placeholder="<?php echo $i18n['events.start_date_placeholder']; ?>" id="start_date" name="start_date"/>
        <input class="form-control form-control-top form-control-bottom picker" type="text" required="" placeholder="<?php echo $i18n['events.end_date_placeholder']; ?>" id="end_date" name="end_date"/>
        <input class="form-control form-control-bottom" type="text" required="" placeholder="<?php echo $i18n['events.description_placeholder']; ?>" id="description" name="description"/><br/><br/>
        <label class="alert alert-info infobox fullwidth"><span class='glyphicon glyphicon-info-sign'></span> <?php echo $i18n['events.auto_generated_shifts_message']; ?></label><br/><br/>
        <label class="fullwidth" for="shifts"><?php echo $i18n['events.shifts']; ?></label>
        <input class="form-control" type="text" value="<?php echo $i18n['events.shift_hours_default']; ?>" id="shift_hours" name="shift_hours"/>
        <br/>
        <label class="fullwidth"><?php echo $i18n['events.shifts']; ?></label>
        <input class="form-control" type="text" value="<?php echo $i18n['events.people_needed_by_shifts_default']; ?>" id="people_needed" name="people_needed"/>
        <br/>
        <input class="btn btn-lg btn-primary btn-block" type="submit" id="submit" name="submit" value="<?php echo $i18n['add']; ?>"/>
        <br/>
        <div class="alert alert-warning">
<?php
if ($message) {
    echo $message;
} else {
    echo $i18n['events.add_new'];
}
?>
        </div>
    </form>
    <script type="text/javascript">
        $("#start_date").datepicker( { format: "yyyy-mm-dd", weekStart: 1 } );
        $("#end_date").datepicker( { format: "yyyy-mm-dd", weekStart: 1 } );
    </script>
</div>
