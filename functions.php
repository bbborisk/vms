<?php
/**
 * Various helper functions
 */

/**
 * Sanitary function for data, used to stop SQL injection
 */
function clean($string) {
    $string = @trim($string);
    if(get_magic_quotes_gpc()) {
        $string = addslashes($string);
    }
    return pg_escape_string($string);
}

/**
 * Get shifts for the target day
 *
 * Gets checkboxes if shifts can still be applied to,
 * otherwise just gets details
 */
function getShifts($target_day) {
    $date = date('Y-m-d', $target_day);
    $today = time();
    $user_id = $_SESSION['user_id'];
    $user_type = $_SESSION['user_type'];
    $result_string = "";
    $select_events = "
        SELECT name, id
        FROM events
        WHERE id IN (
            SELECT DISTINCT(event_id)
            FROM shifts
            WHERE date = '{$date}'
        )";
    if (!$query_result = pg_query($select_events)) {
        die("Error executing query." . pg_last_error());
    } else {
        if (pg_num_rows($query_result) == 0) {
            $result_string = '<br/><br/>&nbsp;';
            return $result_string;
        } else {
            while ($event = pg_fetch_assoc($query_result)) {
                $event_name = $event["name"];
                $event_id = $event["id"];
                $select_shift_hours = "
                    SELECT shifts.id, shifts.start_time, shifts.end_time, shifts.people_needed
                    FROM
                        shifts JOIN
                        events ON shifts.event_id = events.id
                    WHERE
                        events.id = '{$event_id}' AND
                        shifts.date = '{$date}'
                    ORDER BY start_time";
                if (!$query_result_shift_hours = pg_query($select_shift_hours)) {
                    die("Error executing query." . pg_last_error());
                } else {
                    $result_string .= "<br>{$event_name}<br>";
                    while ($shift_hours = pg_fetch_assoc($query_result_shift_hours)) {
                        $shift_id = $shift_hours["id"];
                        $people_needed = $shift_hours["people_needed"];
                        $shift_start_time = strftime('%H:%M', strtotime($shift_hours["start_time"]));
                        $shift_end_time = strftime('%H:%M', strtotime($shift_hours["end_time"]));
                        $number_of_people_applied = 0;
                        $select_shift_numbers = "
                            SELECT count(*) as number_of_people_applied
                            FROM
                                shifts JOIN
                                user_shifts ON shifts.id = user_shifts.shift_id JOIN
                                users ON users.id = user_shifts.user_id
                            WHERE shift_id = '{$shift_id}'";
                        if (!$query_result_people_applied = pg_query($select_shift_numbers)) {
                            die("Error executing query." . pg_last_error());
                        } else {
                            if (pg_num_rows($query_result_people_applied) != 0) {
                                $result = pg_fetch_assoc($query_result_people_applied);
                                $number_of_people_applied = $result["number_of_people_applied"];
                            }
                        }
                        $select_shift = "
                            SELECT shift_id
                            FROM user_shifts
                            WHERE
                                shift_id = '{$shift_id}' AND
                                user_id = '{$user_id}'";
                        if (!$query_result_shift = pg_query($select_shift)) {
                            die("Error executing query." . pg_last_error());
                        } else {
                            if (pg_num_rows($query_result3) == 0) {
                                $applied_to_shift = false;
                            } else {
                                $applied_to_shift = true;
                            }
                            $checkbox_checked_status = ($applied_to_shift) ? 'checked="yes"' : '';
                            $checkbox_shift_past = ($applied_to_shift) ? '<span class="glyphicon glyphicon-ok"></span> ' : '<span class="glyphicon glyphicon-unchecked"></span> ';
                            $max_people_applied = $number_of_people_applied >= $people_needed;
                            $enabled = '';
                            if (!$user_applied_for_this_shift && $max_people_applied) $enabled = ' disabled ';
                            if (strtotime('+1 day', $target_day) > $today) {
                                $result_string .= '<div class="checkbox"><label><input type="checkbox" name="shifts[' . $shift_id . ']" ' . $checkbox_checked_status . $enabled . '/>' . $shift_start_time . ' - ' . $shift_end_time . ' (' . $number_of_people_applied . '/' . $people_needed . ')' . '</label></div>';
                            } else {
                                $result_string .= '<div ><p>' . $checkbox_shift_past . $shift_start_time . ' - ' . $shift_end_time . ' (' . $number_of_people_applied . '/' . $people_needed . ')' . '</p></div>';
                            }
                        }
                    }
                }
            }
            return $result_string;
        }
    }
}

/**
 * Returns a floating point number that indicates how critically
 * in need of paople a shift is, based on how soon the shift is
 * and how many people have applied vs how many are needed
 */
function criticalShift($target_day) {
    $date = date('Y-m-d', $target_day);
    $critical_coefficient = 2.0;
    $select_needed_people = "
        SELECT id, people_needed
        FROM shifts
        WHERE date = '{$date}'";
    if (!$query_result = pg_query($select_needed_people)) {
        die("Error executing query." . pg_last_error());
    } else {
        if (pg_num_rows($query_result) == 0) {
            $number_of_people_needed = 0;
        } else {
            while ($shift = pg_fetch_assoc($query_result)) {
                $shift_id = $shift["id"];
                $number_of_people_needed = $shift["people_needed"];
                $select_shift_numbers = "
                    SELECT count(*) as number_of_people_applied
                    FROM
                        shifts JOIN
                        user_shifts ON shifts.id = user_shifts.shift_id
                    WHERE shifts.id = '{$shift_id}'";
                if (!$query_result_shift_numbers = pg_query($select_shift_numbers)) {
                    die("Error executing query." . pg_last_error());
                } else {
                    if (pg_num_rows($query_result_shift_numbers) != 0) {
                        $result = pg_fetch_assoc($query_result_shift_numbers);
                        $number_of_people_applied = $result["number_of_people_applied"];
                    } else {
                        $number_of_people_applied = 0;
                    }
                }
                $critical_coefficient = min($number_of_people_applied/($people_needed/3.0), $critical_coefficient);
            }
        }
    }
    return $critical_coefficient;
}

// returns info about user as a compact span element
// needed for list of people applied for shift, daily view
function userListItem( $user_id ) {
    global $connection;
    $hide_sensitive_data = ($_SESSION['user_type'] == 3)?1:0;
    $select_user = "SELECT * FROM users WHERE id = '{$user_id}'";
    if (!$query_result = pg_query($select_user)) die("Error executing query." . pg_last_error());
    else {
        // result is user's data
        $result = pg_fetch_assoc($query_result);
        printf("<span> %40s, %15s, %40s, %30s</span>", $result['full_name'], $result['telephone'], $result['email'], $result['occupation']);
    }
}

/**
 * Log user action
 */
function logAdd($log_message) {
    $timestamp = clean(date('Y-m-d H:i:s', time()));
    $log_message = clean($log_message);
    $insert_log_action = "
        INSERT INTO logs(message, created_at)
        VALUES ('{$log_message}', '{$timestamp}')
    ";
    if (!$query_result = pg_query($insert_log_action)) {
        die("Error executing query. " . pg_last_error());
    }
}
