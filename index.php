<?php
/**
 * VMS, Volunteer Management Service
 * Author: Mladen Drobnjaković
 * Year: 2012
 */

// Start the session once
session_start();

require 'functions.php';
require 'db.inc';
require 'translations.inc';

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <title>Volunteer management system</title>

        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" type="text/css" href="css/datepicker.css"/>
        <link rel="stylesheet" type="text/css" href="css/style.css"/>
        <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
        <link rel="icon" type="image/x-icon" href="img/favicon.ico">

        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/functions.js"></script>
        <script type="text/javascript" src="js/datepicker.js"></script>
    </head>
    <body>
<?php
require 'content.php';
?>
        <div class="footer">
<?php
require 'footer.php';
?>
        </div>
    </body>
</html>
