// provera validnosti email adrese
function validateMail(email)
{
    var at_pos = email.indexOf("@");
    var dot_pos = email.lastIndexOf(".");
    if ((at_pos < 1) || (dot_pos < at_pos+2) || (dot_pos+2 >= email.length)) return false;
    return true;
}

// provera login forme
function checkLoginForm() {
    var username = document.getElementById('username').value;
    var password = document.getElementById('password').value;
    if (username == "" || password == "") {
        alert("Молимо вас да унесете имејл и лозинку. Не можете изоставити ниједно од наведеног!");
        return false;
    }
    return true;
}

// provera reset password forme
function checkLostPassForm() {
    var password = document.getElementById('password').value;
    var password2 = document.getElementById('password2').value;
    if (password == password2) {
        return true;
    }
    else {
        alert("Лозинке морају да буду исте!");
        return false;
    }
    return true;
}

// provera new user forme
function checkNewUserForm() {
    var username = document.getElementById('username').value;
    var password = document.getElementById('password').value;
    var password_confirmation = document.getElementById('password_confirmation').value;
    var full_name = document.getElementById('full_name').value;
    var occupation = document.getElementById('occupation').value;
    var telephone = document.getElementById('telephone').value;
    if (username == "" || password == "" || password_confirmation == "" || full_name == "" || telephone == "") {
        alert("Molimo vas da popunite sva polja. Ne možete izostaviti nijedno od navedenog!");
        return false;
    }
    if (password != password_confirmation) {
        alert("Greška pri unosu korisničkog imena i/ili lozinke, ponovljena polja se razlikuju!");
        return false;
    }
    return true;
}

// prikaži/sakrij polja za unos nove lozinke
function toggleNewPasswordFields() {
    var np1 = document.getElementById('new_password');
    var np2 = document.getElementById('new_password_confirmation');
    np1.disabled = !np1.disabled;
    np2.disabled = !np2.disabled;
}

// prikaži alias polje u formi za unos novog korisnika
function showAliasField() {
    var alias = document.getElementById('alias');
    alias.disabled = false;
}

// sakrij alias polje u formi za unos novog korisnika
function hideAliasField() {
    var alias = document.getElementById('alias');
    alias.disabled = true;
}

// obavestenje o prijavi smena
function smeneObavestenje(month) {
    if (proveriSmene(month)) {
        alert("Smene će sada biti prijavljene.");
        return true;
    }
    else {
        alert("Po jednom danu možete prijaviti najviše 2 smene.");
        return false;
    }
    return false;
}

// provera da nije prijavljeno više od 2 smene na dan
function proveriSmene(month) {
    for(i = 1; i <= 31; i++) {
        var name = "2013-" + month + "-";
        if (i<10) name += "0" + i;
        else name += i;
        var name1 = name + ",1";
        var name2 = name + ",2";
        var name3 = name + ",3";
        var cbsmena1 = document.getElementById(name1);
        var cbsmena2 = document.getElementById(name2);
        var cbsmena3 = document.getElementById(name3);
        if (cbsmena1 != null && cbsmena1.checked && cbsmena2 != null && cbsmena2.checked && cbsmena3 != null && cbsmena3.checked) return false;
    }
    return true;
}

function checkNewManifestationForm() {
    return true;
}

