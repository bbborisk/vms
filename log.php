<div class="container whitebg-full">
<?php

// if no user is logged in do not display the page
if (!isset($_SESSION['user_id']) || $_SESSION['user_type'] > 1) {
  header("Location: index.php");
  die();
}

$select_log = "SELECT * FROM logs ORDER BY created_at DESC LIMIT 150";
if (!$result = pg_query($select_log)) die("Error executing query." . pg_last_error());
else {
?>
<table class='table table-striped table-bordered'>
<caption><h3>System activity log</h3></caption>
<tr><td><b>Time</b></td><td><b>Activity</b></td></tr>
<?php
    if (pg_num_rows($result) == 0) {
        echo "<tr><td class='text-center' colspan='2'>No activities</td></tr>";
    }
    else {
        while ($log = pg_fetch_assoc($result)) {
            $message = $log["message"];
            echo "<tr><td>" . $log["created_at"] . "</td><td>" . $message . "</td></tr>";
        }
    }
?>
</table>
<?php
}
?>
</div>
