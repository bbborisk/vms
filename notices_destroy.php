<?php
/**
 * Remove notice page
 */

if (!isset($_SESSION['user_id']) || !(isset($_SESSION['user_type']) && $_SESSION['user_type'] < 3)) {
    header("Location: index.php");
    die();
}

$notice_id = clean($_GET['id']);
$delete_notice = "
    DELETE FROM notices
    WHERE id = '{$notice_id}'
";
if (!$result = pg_query($delete_notice)) {
    die("Error executing query. " . pg_last_error());
} else {
    logAdd("[remove_notice] User '{$_SESSION['username']}' removed a notice (id = {$notice_id}).");
    header("Location: index.php?content=notices_index");
    die();
}
