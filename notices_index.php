<?php
/**
 * Notices page
 */

if (!isset($_SESSION['user_id'])) {
    header("Location: index.php");
    die();
}
$user_can_add_notice = $_SESSION['user_type'] < 3;
?>
<div class="container whitebg">
    <div class="text-center">
        <h2><?php echo $i18n['notices.title']; ?></h2>
    </div>
<?php
if ($user_can_add_notice) {
    if (isset($_POST["submit"])) {
        $title = clean($_POST["title"]);
        $contents = clean(nl2br($_POST["contents"]));
        $user_id = clean($_SESSION['user_id']);
        $timestamp = clean(date('Y-m-d H:i:s', time()));
        $insert_notice = "
            INSERT INTO notices(user_id, title, contents, created_at)
            VALUES ({$user_id}, '{$title}', '{$contents}', '{$timestamp}')
        ";
        if (!$result = pg_query($insert_notice)) {
            die("Error executing query. " . pg_last_error());
        } else {
            logAdd("[add_notice] User '{$_SESSION['username']}' added a notice (title = '{$title}').");
        }
    }
?>
    <form class="form-other form-horizontal" role="form" method="POST" action="">
        <input class="form-control" type="text" required="" placeholder="<?php echo $i18n['notices.notice_title']; ?>" id="title" name="title"/>
        <br/>
        <textarea class="form-control" rows="3" placeholder="<?php echo $i18n['notices.notice_content']; ?>" id="contents" name="contents"></textarea>
        <br/>
        <input class="btn btn-lg btn-primary btn-block" type="submit" id="submit" name="submit" value="<?php echo $i18n['add']; ?>"/>
        <br/>
    </form>
<?php
}
require 'notices_recent.php';
?>
</div>
