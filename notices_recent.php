<?php
/**
 * Notices show last 5
 */

$notices = [];
if (!isset($notice_limit)) {
    $notice_limit = 5;
}
$select_notices = "
    SELECT id, title, contents
    FROM notices
    ORDER BY created_at DESC
    LIMIT {$notice_limit}
";
if (!$result = pg_query($select_notices)) {
    die("Error executing query. " . pg_last_error());
} else {
    if (pg_num_rows($result) != 0) {
        while ($notice = pg_fetch_assoc($result)) {
            array_push($notices, $notice);
        }
    }
}
if (empty($notices)) {
?>
    <div class="alert alert-info"><?php echo $i18n['notices.empty']; ?></div>
<?php
} else {
    $user_is_admin = $_SESSION['user_type'] < 2;
    foreach ($notices as $notice) {
        $id = $notice["id"];
        $title = $notice["title"];
        $contents = $notice["contents"];
?>
    <div class="alert alert-info">
        <h3><?php echo $title; ?></h3>
        <p><?php echo $contents; ?></p>
<?php
        if ($user_is_admin) {
?>
        <br/>
        <a class="btn btn-default btn-xs" href="index.php?content=notices_destroy&id=<?php echo $id; ?>">
            <span class='glyphicon glyphicon-trash'> <?php echo $i18n['remove']; ?></span>
        </a>
<?php
        }
?>
    </div>
<?php
    }
}
