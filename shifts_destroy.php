<?php
/**
 * Remove shift page
 */

if (!isset($_SESSION['user_id']) || !(isset($_SESSION['user_type']) && $_SESSION['user_type'] == 1)) {
    header("Location: index.php");
    die();
}

$redirect_back_url = urldecode($_GET["redirect_back_url"]);
if (isset($_GET["shift_id"])) {
    $shift_id = $_GET["shift_id"];
}
if ($shift_id) {
    // Dependent destroy
    // TODO: Maybe ensure this by DB design
    // by adding cascade destroy for foreign keys
    $delete_user_shifts = "
        DELETE FROM user_shifts
        WHERE shift_id = {$shift_id}";
    if (!$query_result = pg_query($delete_user_shifts)) {
        die("Error executing query. " . pg_last_error());
    }

    $delete_shift = "
        DELETE FROM shifts
        WHERE id = {$shift_id}";
    if (!$query_result = pg_query($delete_shift)) {
        die("Error executing query. " . pg_last_error());
    } else {
        logAdd("[manual_remove_shift] User '{$_SESSION['username']}' manualy deleted a shift (shift_id = '{$shift_id}').");
        header("Location: {$redirect_back_url}");
        die();
    }
}
