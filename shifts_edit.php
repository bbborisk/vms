<?php
/**
 * Edit shift page
 */

if (isset($_GET["shift_id"])) {
    $shift_id = $_GET["shift_id"];
}
$redirect_back_url = urldecode($_GET["redirect_back_url"]);

if ($shift_id && (!isset($_SESSION['user_id']) || !(isset($_SESSION['user_type']) && $_SESSION['user_type'] == 1))) {
    header("Location: index.php");
    die();
}

if (isset($_POST["submit"])) {
    $date = clean($_POST["date"]);
    $start_time = clean($_POST["start_time"]);
    $end_time = clean($_POST["end_time"]);
    $people_needed = clean($_POST["people_needed"]);
    $update_shift = "
        UPDATE shifts
        SET
            date = '{$date}',
            start_time = '{$start_time}',
            end_time = '{$end_time}',
            people_needed = {$people_needed}
        WHERE id = {$shift_id}";
    if (!$result_shift = pg_query($update_shift)) {
        die("Error executing query." . pg_last_error());
    } else {
        logAdd("[update_shift] User '{$_SESSION['username']}' changed shift (id = {$shift_id}).");
        header("Location: {$redirect_back_url}");
        die();
    }
}
$select_shift = "
    SELECT *
    FROM shifts
    WHERE id = {$shift_id}";
if (!$result = pg_query($select_shift)) {
    die("Error executing query." . pg_last_error());
} else {
    if (pg_num_rows($result) != 0) {
        $shift = pg_fetch_assoc($result);
    } else {
        header("Location: {$redirect_back_url}");
        die();
    }
}
?>
<div class="container whitebg">
    <form class="form-other" role="form" method="POST" action="">
        <h2><?php echo $i18n['shifts.edit']; ?></h2>
        <br/>
        <input class="form-control form-control-top picker" type="text" required="" value="<?php echo $shift['date']; ?>" id="date" name="date"/>
        <input class="form-control form-control-top form-control-bottom" type="text" required="" value="<?php echo strftime('%H:%M', strtotime($shift['start_time'])); ?>" id="start_time" name="start_time"/>
        <input class="form-control form-control-top form-control-bottom" type="text" required="" value="<?php echo strftime('%H:%M', strtotime($shift['end_time'])); ?>" id="end_time" name="end_time"/>
        <input class="form-control form-control-bottom" type="text" value="<?php echo $shift['people_needed']; ?>" id="people_needed" name="people_needed"/>
        <br/>
        <input class="btn btn-lg btn-primary btn-block" type="submit" id="submit" name="submit" value="<?php echo $i18n['save']; ?>"/>
        <br/>
        <div class="alert alert-warning">
            <?php echo $i18n['shifts.edit']; ?>
        </div>
    </form>
    <script type="text/javascript">
        $("#date").datepicker( { format: "yyyy-mm-dd", weekStart: 1 } );
    </script>
</div>
