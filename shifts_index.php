<?php
/**
 * Show event's shifts page
 */

if (isset($_GET["event_id"])) {
    $event_id = $_GET["event_id"];
}

if ($event_id && (!isset($_SESSION['user_id']) || !(isset($_SESSION['user_type']) && $_SESSION['user_type'] == 1))) {
    header("Location: index.php");
    die();
}

$select_shifts = "
    SELECT
        shifts.*,
        (shifts.end_time - shifts.start_time) AS duration,
        events.name AS event_name
    FROM
        shifts JOIN
        events ON shifts.event_id = events.id
    WHERE shifts.event_id = {$event_id}
    ORDER BY shifts.date, shifts.start_time";
if (!$result = pg_query($select_shifts)) {
    die("Error executing query." . pg_last_error());
} else {
?>
<div class="whitebg-mid">
    <table class='table table-striped table-bordered'>
        <thead>
            <tr>
                <th>
                    <b><?php echo $i18n['shifts.number_sign']; ?></b>
                </th>
                <th>
                    <b><?php echo $i18n['shifts.event_name']; ?></b>
                </th>
                <th>
                    <b><?php echo $i18n['shifts.date']; ?></b>
                </th>
                <th>
                    <b><?php echo $i18n['shifts.start_time']; ?></b>
                </th>
                <th>
                    <b><?php echo $i18n['shifts.end_time']; ?></b>
                </th>
                <th>
                    <b><?php echo $i18n['shifts.duration']; ?></b>
                </th>
                <th>
                    <b><?php echo $i18n['shifts.people_needed']; ?></b>
                </th>
                <th>
                    <b><?php echo $i18n['shifts.action']; ?></b>
                </th>
            </tr>
        </thead>
        <tbody>
<?php
    if (pg_num_rows($result) == 0) {
?>
            <tr>
                <td class="text-center" colspan="6"><?php echo $i18n['shifts.empty']; ?></td>
            </tr>
<?php
    } else {
        while ($shift = pg_fetch_assoc($result)) {
            $shift_duration = strtotime($shift['duration']);
            $duration_float = (float)strftime('%H', $shift_duration) + (float)strftime('%M', $shift_duration)/60.0;
?>
            <tr>
                <td><?php echo $shift["id"]; ?></td>
                <td><?php echo $shift["event_name"]; ?></td>
                <td><?php echo $shift["date"]; ?></td>
                <td><?php echo strftime('%H:%M', strtotime($shift['start_time'])); ?></td>
                <td><?php echo strftime('%H:%M', strtotime($shift['end_time'])); ?></td>
                <td><?php echo number_format($duration_float, 2) . ' ' . $i18n['shifts.hours']; ?></td>
                <td><?php echo $shift["people_needed"]; ?></td>
                <td>
                    <a class="btn btn-default btn-xs" href="index.php?content=shifts_destroy&shift_id=<?php echo $shift['id']; ?>&redirect_back_url=<?php echo urlencode($_SERVER['REQUEST_URI']) ?>"><span class="glyphicon glyphicon-trash"> <?php echo $i18n['delete']; ?></span></a>
                    <a class="btn btn-default btn-xs" href="index.php?content=shifts_edit&shift_id=<?php echo $shift['id']; ?>&redirect_back_url=<?php echo urlencode($_SERVER['REQUEST_URI']) ?>"><span class="glyphicon glyphicon-edit"> <?php echo $i18n['edit']; ?></span></a>
                </td>
            </tr>
<?php
        }
    }
?>
            <tr>
                <td class="text-left" colspan="8">
                    <a class="btn btn-default btn-xs" href="index.php?content=shifts_new&event_id=<?php echo $event_id; ?>&redirect_back_url=<?php echo urlencode($_SERVER['REQUEST_URI']) ?>"><span class="glyphicon glyphicon-plus"> <?php echo $i18n['shifts.add_new']; ?></span></a>
                </td>
            </tr>
        </tbody>
    </table>
<?php
}
?>
</div>
