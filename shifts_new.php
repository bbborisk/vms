<?php
/**
 * New shift page
 */

if (isset($_GET["event_id"])) {
    $event_id = $_GET["event_id"];
}
$redirect_back_url = urldecode($_GET["redirect_back_url"]);

if ($event_id && (!isset($_SESSION['user_id']) || !(isset($_SESSION['user_type']) && $_SESSION['user_type'] == 1))) {
    header("Location: index.php");
    die();
}

if (isset($_POST["submit"])) {
    $date = clean($_POST["date"]);
    $start_time = clean($_POST["start_time"]);
    $end_time = clean($_POST["end_time"]);
    $people_needed = clean($_POST["people_needed"]);
    $insert_shift = "
        INSERT INTO shifts(event_id, date, start_time, end_time, people_needed)
        VALUES ({$event_id}, '{$date}', '{$start_time}', '{$end_time}', {$people_needed})";
    if (!$result_shift = pg_query($insert_shift)) {
        die("Error executing query." . pg_last_error());
    } else {
        logAdd("[new_shift] User '{$_SESSION['username']}' added a new shift to event (event_id = {$event_id}).");
        header("Location: {$redirect_back_url}");
        die();
    }
}
?>
<div class="container whitebg">
    <form class="form-other" role="form" method="POST" action="">
        <h2><?php echo $i18n['shifts.add_new']; ?></h2>
        <br/>
        <input class="form-control form-control-top picker" type="text" required="" placeholder="<?php echo $i18n['shifts.date_placeholder']; ?>" id="date" name="date"/>
        <input class="form-control form-control-top form-control-bottom" type="text" required="" placeholder="<?php echo $i18n['shifts.start_time_placeholder']; ?>" id="start_time" name="start_time"/>
        <input class="form-control form-control-top form-control-bottom" type="text" required="" placeholder="<?php echo $i18n['shifts.end_time_placeholder']; ?>" id="end_time" name="end_time"/>
        <input class="form-control form-control-bottom" type="text" placeholder="<?php echo $i18n['shifts.people_needed_placeholder']; ?>" id="people_needed" name="people_needed"/>
        <br/>
        <input class="btn btn-lg btn-primary btn-block" type="submit" id="submit" name="submit" value="<?php echo $i18n['add']; ?>"/>
        <br/>
        <div class="alert alert-warning">
            <?php echo $i18n['shifts.add_new']; ?>
        </div>
    </form>
    <script type="text/javascript">
        $("#date").datepicker( { format: "yyyy-mm-dd", weekStart: 1 } );
    </script>
</div>
