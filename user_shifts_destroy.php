<?php
/** Remove user's shift page
 */

if (!isset($_SESSION['user_id']) || !(isset($_SESSION['user_type']) && $_SESSION['user_type'] < 3)) {
    header("Location: index.php");
    die();
}

$redirect_back_url = urldecode($_GET["redirect_back_url"]);
if (isset($_GET["user_shift_id"])) {
    $user_shift_id = $_GET["user_shift_id"];
}
if ($user_shift_id) {
    $delete_user_shift = "
        DELETE FROM user_shifts
        WHERE id = {$user_shift_id}";
    if (!$query_result = pg_query($delete_user_shift)) {
        die("Error executing query." . pg_last_error());
    } else {
        logAdd("[manual_remove_shift] User '{$_SESSION['username']}' manualy deleted a user shift (user_shift_id = '{$user_shift_id}').");
        header("Location: {$redirect_back_url}");
        die();
    }
}
