<?php
/**
 * Add user shift action and page
 */

// if no user is logged in do not display the page
if (!isset($_SESSION['user_id'])) {
    header("Location: index.php");
    die();
}

// set shift_id, user_id for adding
if (isset($_GET["shift_id"])) {
    $shift_id = $_GET["shift_id"];
}
if (isset($_GET["user_id"])) {
    $user_id = $_GET["user_id"];
} else {
    $user_id = false;
}
if (isset($_GET["search_name"])) {
    $search_name = $_GET["search_name"];
} else {
    $search_name = false;
}

if (!$search_name && !$user_id) {
?>
<div class='container whitebg'>
    <h2><?php echo $i18n['shifts.find_user']; ?></h2>
    <form class='form-group' method='GET' action=''>
        <input id='shift_id' name='shift_id' type='hidden' value='<?php echo $shift_id; ?>' />
        <label for='search_name'><?php echo $i18n['shifts.search_name']; ?></label>
        <input class='form-control' id='search_name' name='search_name' type='text' required="" placeholder="<?php echo $i18n['shifts.name_placeholder']; ?>" /><br/>
        <input class='btn btn-sm btn-primary btn-block' id='submit' name='submit' type='submit' value='<?php echo $i18n['shifts.search']; ?>'/>
    </form>
</div>
<?php
} else {
    if ($user_id) {
?>
<div class="container whitebg-full">
<?php
        $insert_shift = "INSERT INTO user_shifts(shift_id, user_id) VALUES ('{$shift_id}', '{$user_id}')";
        if (!$result = pg_query($insert_shift)) {
            die("Error executing query." . pg_last_error());
        } else {
            logAdd("[manual_add_user_shift] User '{$_SESSION['username']}' manualy added a shift (shift_id = '{$shift_id}', user_id = '{$user_id}').");
            echo "<div class='alert alert-info'>" . $i18n['shifts.added'] . "</div>";
        }
    } else {
        $select_users = "SELECT full_name, id FROM users WHERE UPPER(full_name) LIKE '%" . strtoupper($search_name) . "%'";
        if (!$result = pg_query($select_users)) {
            die("Error executing query." . pg_last_error());
        } else {
            echo "<div class='tablecontainer whitebg-full text-center'>";
            echo "  <h2>" . $i18n['shifts.search_results_for'] . " '" . $search_name . "'</h2>";
            echo "  <table class='table table-striped table-bordered table-condensed'>";
            echo "      <tr>";
            echo "          <td><b>" . $i18n['users.full_name'] . "</b></td>";
            echo "          <td><b>" . $i18n['users.telephone'] . "</b></td>";
            echo "          <td><b>" . $i18n['users.email'] . "</b></td>";
            echo "          <td><b>" . $i18n['users.occupation'] . "</b></td>";
            echo "          <td><b>" . $i18n['users.personal_id_number'] . "</b></td>";
            echo "          <td><b>" . $i18n['users.address'] . "</b></td>";
            echo "          <td class='hidden'><b>" . $i18n['action'] . "</b></td>";
            echo "      </tr>";
            while ($user = pg_fetch_assoc($result)) {
                echo "  <tr>";
                userTableItem($user['id']);
                echo '      <td><a type="button" class="btn btn-default btn-xs" href="index.php?content=user_shifts_new&amp;shift_id=' . $shift_id  . '&amp;user_id=' . $user['id'] . '"><span class="glyphicon glyphicon-plus"> ' . $i18n['add'] . '</span></a><br/></td>';
                echo '  </tr>';
            }
            echo "  </table>";
            echo "</div>";
        }
    }
}
