<?php
/**
 * Sum shifts page
 */

if (!isset($_SESSION['user_id']) || $_SESSION['user_type'] == 3) {
    header("Location: index.php");
    die();
}

$select_user_start_shifts = "
    SELECT users.*, 0 AS number_of_shifts
    FROM users";
$shifts = [];
$users = [];

if (!$result = pg_query($select_user_start_shifts)) {
    die("Error executing query." . pg_last_error());
} else {
    while ($user_start_shifts = pg_fetch_assoc($result)) {
        $user_id = $user_start_shifts['id'];
        $users[$user_id] = [
            'id' => $user_start_shifts['id'],
            'full_name' => $user_start_shifts['full_name'],
            'telephone' => $user_start_shifts['telephone'],
            'personal_id_number' => $user_start_shifts['personal_id_number'],
            'occupation' => $user_start_shifts['occupation'],
            'address' => $user_start_shifts['address'],
            'email' => $user_start_shifts['email']
        ];
        $number_of_shifts = $user_start_shifts['number_of_shifts'];
        $hours = 0.0;
        $shifts[$user_id] = [
            'hours' => $hours,
            'shifts' => $number_of_shifts
        ];
    }
}

$events = null;

$select_events = "
    SELECT *
    FROM events";
if (!$result = pg_query($select_events)) {
    die("Error executing query." . pg_last_error());
} else {
    if (pg_num_rows($result) != 0) {
        $events = [];
        while ($event = pg_fetch_assoc($result)) {
            $event_id = $event['id'];
            array_push($events, ['name' => $event['name'], 'id' => $event['id']]);
        }
    }
}

if (!is_null($events)) {
    $last_event_id = end($events)['id'];
    $user_type_query_part = " AND users.type = 3";
    $user_type = 3;
    $show_all = false;

    if (isset($_POST["submit"])) {
        $event_id = clean($_POST['event_id']);
        $user_type = clean($_POST['user_type']);
        switch ($user_type) {
        case 0:
            $user_type_query_part = "";
            break;
        case 1:
            $user_type_query_part = " AND users.type = 1";
            break;
        case 2:
            $user_type_query_part = " AND users.type = 2";
            break;
        case 3:
            $user_type_query_part = " AND users.type = 3";
            break;
        }
        $show_all = isset($_POST['show_all']) && $_POST['show_all'] == 1;
    }
    if (!isset($event_id)) {
        $event_id = $last_event_id;
    }

    $select_shifts = "
        SELECT
            users.*,
            shifts.id AS shift_id,
            (shifts.end_time - shifts.start_time) AS shift_hours
        FROM
            users JOIN
            user_shifts ON user_shifts.user_id = users.id JOIN
            shifts ON shifts.id = user_shifts.shift_id
        WHERE
            shifts.event_id = '{$event_id}' {$user_type_query_part}";
    if (!$result = pg_query($select_shifts)) {
        die("Error executing query." . pg_last_error());
    } else {
        while ($user_shifts = pg_fetch_assoc($result)) {
            $shift_hours = strtotime($user_shifts["shift_hours"]);
            $shift_hours_float = (float)strftime('%H', $shift_hours) + (float)strftime('%M', $shift_hours)/60.0;
            $user_id = $user_shifts['id'];
            $shifts[$user_id]['hours'] += $shift_hours_float;
            $shifts[$user_id]['shifts']++;
        }
    }
}
$table_class = (is_null($events)) ? 'whitebg' : 'whitebg-full';
?>
<div class="tablecontainer <?php echo $table_class; ?>">
<?php
if (is_null($events)) {
?>
    <div class="alert alert-info"><?php echo $i18n['events.empty']; ?></div>
<?php
} else {
?>
    <form class="form-other form-horizontal" role="form" method="POST" action="">
        <div class="form-group">
            <div class="col-xs-6">
                <select class="form-control" id="event_id" name="event_id">
<?php
    foreach ($events as $event) {
        $selected = ($event['id'] == $event_id) ? 'selected' : '';
        $user_type_0_selected = ($user_type == 0) ? 'selected' : '';
        $user_type_1_selected = ($user_type == 1) ? 'selected' : '';
        $user_type_2_selected = ($user_type == 2) ? 'selected' : '';
        $user_type_3_selected = ($user_type == 3) ? 'selected' : '';
        $show_all_checked = ($show_all) ? 'checked' : '';
?>
                    <option value="<?php echo $event['id']; ?>" <?php echo $selected; ?>><?php echo $event['name']; ?></option>
<?php
    }
?>
                </select>
            </div>
            <div class="col-xs-6">
                <select class="form-control" id="user_type" name="user_type">
                    <option value="0" <?php echo $user_type_0_selected; ?>><?php echo $i18n['users.any_type']; ?></option>
                    <option value="1" <?php echo $user_type_1_selected; ?>><?php echo $i18n['users.administrators_type']; ?></option>
                    <option value="2" <?php echo $user_type_2_selected; ?>><?php echo $i18n['users.demonstrators_type']; ?></option>
                    <option value="3" <?php echo $user_type_3_selected; ?>><?php echo $i18n['users.volunteers_type']; ?></option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-6">
                <div class="checkbox">
                    <label for="show_all"><?php echo $i18n['users.show_with_0_shifts']; ?></label>
                    <input id="show_all" name="show_all" type="checkbox" value="1" <?php echo $show_all_checked; ?> />
                </div>
            </div>
            <div class="col-xs-6">
                <input class="btn btn-primary btn-block" type="submit" id="submit" name="submit" value="<?php echo $i18n['calculate']; ?>"/>
            </div>
        </div>
    </form>
    <table class="table table-striped table-bordered table-condensed">
        <thead>
            <tr>
            <th><b><?php echo $i18n['users.number_sign']; ?></b></th>
                <th><b><?php echo $i18n['users.full_name']; ?></b></th>
                <th><b><?php echo $i18n['users.telephone']; ?></b></th>
                <th><b><?php echo $i18n['users.email']; ?></b></th>
                <th><b><?php echo $i18n['users.occupation']; ?></b></th>
                <th><b><?php echo $i18n['users.personal_id_number']; ?></b></th>
                <th><b><?php echo $i18n['users.address']; ?></b></th>
                <th><b><?php echo $i18n['users.hours_shifts']; ?></b></th>
            </tr>
        </thead>
        <tbody>
<?php
    foreach ($shifts as $user_id => $user_shifts) {
        if ($user_shifts['shifts'] != 0 || $show_all) {
?>
            <tr>
                <td><?php echo $user_id; ?></td>
                <td><?php echo $users[$user_id]['full_name']; ?></td>
                <td><?php echo $users[$user_id]['telephone']; ?></td>
                <td><?php echo $users[$user_id]['email']; ?></td>
                <td><?php echo $users[$user_id]['occupation']; ?></td>
                <td><?php echo $users[$user_id]['personal_id_number']; ?></td>
                <td><?php echo $users[$user_id]['address']; ?></td>
                <td><?php echo $user_shifts['hours']; ?> hours / <?php echo $user_shifts['shifts']; ?> shifts</td>
            </tr>
<?php
        }
    }
?>
    </table>
<?php
}
?>
</div>
