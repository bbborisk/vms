<?php
/**
 * Edit user info
 */

if (!isset($_SESSION['user_id'])) {
    header("Location: index.php");
    die();
}

$wrong_username = false;
$wrong_password = false;
$wrong_password_confirmation = false;
$success = false;

$user_id = $_SESSION['user_id'];

if (isset($_POST["submit"])) {
    $password_salt = $_secrets['password_salt'];
    $username = $_POST["username"];
    $password = $_POST["password"];
    $change_password = isset($_POST["change_password"]);
    $full_name = $_POST["full_name"];
    $occupation = $_POST["occupation"];
    $telephone = $_POST["telephone"];
    $personal_id_number = $_POST["personal_id_number"];
    $address = $_POST["address"];
    $password_hash = md5($password . $password_salt);
    if ($change_password) {
        $new_password = $_POST["new_password"];
        $new_password_confirmation = $_POST["new_password_confirmation"];
        if ($new_password == $new_password_confirmation) {
            $new_password_hash = md5($new_password . $password_salt);
            $change_password_query_part = "password_hash = '{$new_password_hash}', ";
        } else {
            $wrong_password_confirmation = true;
        }
    } else {
        $change_password_query_part = "";
    }
    if (!$wrong_password_confirmation) {
        $select_user = "SELECT * FROM users WHERE id = '{$user_id}'";
        if (!$result = pg_query($select_user)) {
            die("Error executing query." . pg_last_error());
        } else {
            $user = pg_fetch_assoc($result);
        }
        if ($password_hash == $user["password_hash"]) {
            if ($username != $user["email"]) {
                $change_username_query_part = "email = '{$username}', ";
                $search_new_username = "SELECT * FROM users WHERE email = '{$username}'";
                if (!$result = pg_query($search_new_username)) {
                    die("Error executing query." . pg_last_error());
                } else {
                    if (pg_num_rows($result) != 0) {
                        $wrong_username = true;
                    }
                }
            } else {
                $change_username_query_part = "";
            }
            $update_user = "UPDATE users SET " . $change_username_query_part . $change_password_query_part . " full_name = '{$full_name}', telephone = '{$telephone}', occupation = '{$occupation}', personal_id_number = '{$personal_id_number}', address = '{$address}' WHERE id = '{$user_id}'";
            if (!$result = pg_query($update_user)) {
                die("Error executing query." . pg_last_error());
            } else {
                logAdd("[edit_user_info] User '{$_SESSION['username']}' changed profile info (email = \'{$username}\', full_name = \'{$full_name}\').");
                $_SESSION['username'] = $username;
                $_SESSION['user_full_name'] = $full_name;
                $success = true;
                header("Location: index.php?content=users_edit");
                die();
            }
        } else {
            $wrong_password = true;
        }
    }
}
$select_user = "SELECT * FROM users WHERE id = '{$user_id}'";
if (!$result = pg_query($select_user)) {
    die("Error executing query." . pg_last_error());
} else {
    $user = pg_fetch_assoc($result);
}
?>
<div class="container whitebg-mid">
    <form class="form-other form-horizontal" role="form" method="POST" action="" onsubmit="return checkEditUserForm()">
        <h2 class="text-center"><?php echo $i18n['users.edit_info'] ?></h2>
        <br/>
        <div class="form-group form-group-top">
            <label for="username" class="col-sm-4 control-label"><?php echo $i18n['users.email'] ?></label>
            <div class="col-sm-8">
                <input class="form-control form-control-top" type="username" required="" id="username" name="username" value="<?php echo $user["email"]?>"/>
            </div>
        </div>
        <div class="form-group form-group-top form-group-bottom">
            <label for="full_name" class="col-sm-4 control-label"><?php echo $i18n['users.full_name'] ?></label>
            <div class="col-sm-8">
                <input class="form-control form-control-top form-control-bottom" type="text" required="" id="full_name" name="full_name" value="<?php echo $user["full_name"]?>"/>
            </div>
        </div>
        <div class="form-group form-group-top form-group-bottom">
            <label for="occupation" class="col-sm-4 control-label"><?php echo $i18n['users.occupation'] ?></label>
            <div class="col-sm-8">
                <input class="form-control form-control-top form-control-bottom" type="text" required="" id="occupation" name="occupation" value="<?php echo $user["occupation"]?>"/>
            </div>
        </div>
        <div class="form-group form-group-top form-group-bottom">
            <label for="telephone" class="col-sm-4 control-label"><?php echo $i18n['users.telephone'] ?></label>
            <div class="col-sm-8">
                <input class="form-control form-control-top form-control-bottom" type="text" required="" id="telephone" name="telephone" value="<?php echo $user["telephone"]?>"/>
            </div>
        </div>
        <div class="form-group form-group-top form-group-bottom">
            <label for="personal_id_number" class="col-sm-4 control-label"><?php echo $i18n['users.personal_id_number'] ?></label>
            <div class="col-sm-8">
                <input class="form-control form-control-top form-control-bottom" type="text" required="" id="personal_id_number" name="personal_id_number" value="<?php echo $user["personal_id_number"]?>"/>
            </div>
        </div>
        <div class="form-group form-group-bottom">
            <label for="address" class="col-sm-4 control-label"><?php echo $i18n['users.address'] ?></label>
            <div class="col-sm-8">
                <input class="form-control form-control-bottom" type="text" required="" id="address" name="address" value="<?php echo $user["address"]?>"/>
            </div>
        </div>
        <div class="checkbox">
            <input id="change_password" name="change_password" type="checkbox" value="1" onclick="toggleNewPasswordFields()"/>
            <label for="change_password"><?php echo $i18n['users.set_new_password'] ?></label>
        </div>
        <br/>
        <input class="form-control form-control-top" type="password" placeholder="<?php echo $i18n['users.new_password_placeholder'] ?>" id="new_password" name="new_password" disabled/>
        <input class="form-control form-control-bottom" type="password" placeholder="<?php echo $i18n['users.new_password_confirmation_placeholder'] ?>" id="new_password_confirmation" name="new_password_confirmation" disabled/>
        <br/>
        <label class="alert alert-warning infobox fullwidth" for="password">
            <span class='glyphicon glyphicon-warning-sign'></span> <?php echo $i18n['users.change_info_warning'] ?>
        </label>
        <input class="form-control" type="password" placeholder="<?php echo $i18n['users.current_password_placeholder'] ?>" id="password" name="password"/>
        <br/>
        <input class="btn btn-lg btn-primary btn-block" type="submit" id="submit" name="submit" value="<?php echo $i18n['save'] ?>"/>
        <br/>
        <div class="alert alert-warning">
<?php
if ($wrong_password_confirmation) {
    echo $i18n['users.wrong_password_confirmation'];
} elseif ($wrong_username) {
    echo $i18n['users.wrong_username'];
} elseif ($wrong_password) {
    echo $i18n['users.wrong_password'];
} elseif ($success) {
    echo $i18n['success'];
} else {
    echo $i18n['users.edit_info'];
}
?>
        </div>
    </form>
</div>
