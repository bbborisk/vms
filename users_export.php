<?php
/**
 * User list in CSV format
 */

// Try to send it compressed
if(!ob_start("ob_gzhandler")) ob_start();

// Get session
session_start();

// Redirect if not permitted
if (!isset($_SESSION['user_id']) || $_SESSION['user_type'] > 1) {
    header("Location: index.php");
    die();
}

// Connect to database
require 'db.inc';

require 'translations.inc';

$column_names = [
    'id' => $i18n['users.number_sign'],
    'type' => $i18n['users.type'],
    'email' => $i18n['users.email'],
    'full_name' => $i18n['users.full_name'],
    'telephone' => $i18n['users.telephone'],
    'occupation' => $i18n['users.occupation'],
    'personal_id_number' => $i18n['users.personal_id_number'],
    'address' => $i18n['users.address'],
    'authorized' => $i18n['users.authorized']
];

/**
 * Helper function to map column name to translation text
 */
function map_column_names($input) {
    global $column_names;

    return isset($column_names[$input]) ? $column_names[$input] : $input;
}

// Filename for download
$filename = "user_list_" . date('Y-m-d') . ".csv";
header("Content-Disposition: attachment; filename=\"$filename\"");
header("Content-Type: text/csv; charset=UTF-8");

// Write to output
$out = fopen("php://output", 'w');

$header_printed_flag = false;
$select_users = "
    SELECT id, type, email, full_name, telephone, occupation, personal_id_number, address, authorized
    FROM users
    ORDER BY type, full_name";
if (!$result = pg_query($select_users)) {
    die("Error executing query." . pg_last_error());
} else {
    if (pg_num_rows($result) == 0) {
        echo $i18n['users.empty'];
    } else {
        while ($user = pg_fetch_assoc($result)) {
            $row = [
                $user['id'],
                $user['type'],
                $user['email'],
                $user['full_name'],
                $user['telephone'],
                $user['occupation'],
                $user['personal_id_number'],
                $user['address'],
                $user['authorized']
            ];
            if (!$header_printed_flag) {
                $first_line = array_map("map_column_names", array_keys($user));
                fputcsv($out, $first_line, ',', '"');
                $header_printed_flag = true;
            }
            fputcsv($out, array_values($row), ',', '"');
        }
    }
}

fclose($out);
exit;
