<?php
/**
 * User list page
 */

if (!isset($_SESSION['user_id']) || $_SESSION['user_type'] > 1) {
    header("Location: index.php");
    die();
}

$select_users = "
    SELECT *
    FROM users
    ORDER BY type, full_name";
if (!$result = pg_query($select_users)) {
    die("Error executing query." . pg_last_error());
} else {
    if (pg_num_rows($result) != 0) {
?>
<div class="whitebg-full">
    <a class="pull-right" href="users_export.php" target="_blank"><span class='glyphicon glyphicon-download'></span> <?php echo $i18n['users.download_user_list']; ?></a>
    <br/>
    <br/>
    <table class="table table-striped table-bordered table-condensed">
        <thead>
            <tr>
                <th><b><?php echo $i18n['users.number_sign']; ?></b></th>
                <th><b><?php echo $i18n['users.type']; ?></b></th>
                <th><b><?php echo $i18n['users.full_name']; ?></b></th>
                <th><b><?php echo $i18n['users.telephone']; ?></b></th>
                <th><b><?php echo $i18n['users.email']; ?></b></th>
                <th><b><?php echo $i18n['users.occupation']; ?></b></th>
                <th><b><?php echo $i18n['users.personal_id_number']; ?></b></th>
                <th><b><?php echo $i18n['users.address']; ?></b></th>
                <th><b><?php echo $i18n['users.alias']; ?></b></th>
                <th><b><?php echo $i18n['users.authorized']; ?></b></th>
                <th><b><?php echo $i18n['users.date_of_registration']; ?></b></th>
            </tr>
        </thead>
        <tbody>
<?php
        while ($user = pg_fetch_assoc($result)) {
            switch ($user['type']) {
            case 3:
                $user_type = $i18n['users.volunteer'];
                break;
            case 2:
                $user_type = $i18n['users.demonstrator'];
                break;
            case 1:
                $user_type = $i18n['users.administrator'];
                break;
            default:
                $user_type = $i18n['users.unknown'];
                break;
            }
            $authorized = ($user['authorized'] == 't') ? $i18n['yes'] : $i18n['no'];
            $date = date('Y-m-d', strtotime($user['created_at']));
?>
            <tr>
                <td><?php echo $user['id']; ?></td>
                <td><?php echo $user_type; ?></td>
                <td><?php echo $user['full_name']; ?></td>
                <td><?php echo $user['telephone']; ?></td>
                <td><?php echo $user['email']; ?></td>
                <td><?php echo $user['occupation']; ?></td>
                <td><?php echo $user['personal_id_number']; ?></td>
                <td><?php echo $user['address']; ?></td>
                <td><?php echo $user['alias']; ?></td>
                <td><?php echo $authorized; ?></td>
                <td><?php echo $date; ?></td>
            </tr>
<?php
        }
?>
    </table>
<?php
    } else {
?>
    <div class="tablecontainer whitebg">
        <div class="alert alert-info">
            <?php echo $i18n['users.empty']; ?>
        <div>
    </div>';
<?php
    }
}
?>
</div>
