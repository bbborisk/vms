<?php
/**
 * Login page
 */

$wrong_username = false;
$wrong_password = false;

if (isset($_POST["submit"])) {
    $password_salt = $_secrets['password_salt'];
    $username = clean($_POST["username"]);
    $password = clean($_POST["password"]);
    $password_hash = md5($password . $password_salt);

    $select_user = "
        SELECT *
        FROM users
        WHERE
            email = '{$username}' OR
            alias = '{$username}'";
    if (!$result = pg_query($select_user)) {
        die("Error executing query." . pg_last_error());
    } else {
        if (pg_num_rows($result) == 0) {
            $wrong_username = true;
        }
        else {
            $user = pg_fetch_assoc($result);
            if ($user['password_hash'] === $password_hash) {
                $_SESSION['username'] = $username;
                $_SESSION['user_id'] = $user['id'];
                $_SESSION['user_type'] = $user['type'];
                $_SESSION['user_full_name'] = $user['full_name'];
                logAdd("[login] User '{$_SESSION['username']}' has logged in.");
                header("Location: index.php");
                die();
            } else {
                $wrong_password = true;
                // TODO: Add ip ban for 1 hour after 3 failed attempts for the same user
                logAdd("Somebody tried to login as '{$username}' using passowrd '{$password}'");
            }
        }
    }
}

?>
<form class="form-signin" role="form" method="POST" action="" onsubmit="return checkLoginForm()">
    <div class="loginlogo center-block">
        <img src="img/logo.png"/>
    </div>
    <h2 class="form-signin-heading text-center"><?php echo $i18n['login_title'] ?></h2>
    <br/>
    <input class="form-control" type="username" autofocus="" required="" placeholder="<?php echo $i18n['login.email_placeholder'] ?>" id="username" name="username"/>
    <input class="form-control" type="password" required="" placeholder="<?php echo $i18n['login.password_placeholder'] ?>" id="password" name="password"/>
    <input class="btn btn-lg btn-primary btn-block" type="submit" id="submit" name="submit" value="<?php echo $i18n['login.login_action'] ?>"/>
    <br/>
<?php
if ($wrong_username || $wrong_password) {
?>
    <div class="alert alert-warning">
<?php
    if ($wrong_username) {
        echo $i18n['login.wrong_password_warning'];
    } elseif ($wrong_password) {
        echo $i18n['login.wrong_username_warning'];
?>
        <br/>
        <a href="index.php?content=users_password_reset&username=<?php echo $username ?>"><?php echo $i18n['login.forgot_password'] ?></a>
<?php
    }
?>
    </div>
<?php
}
?>
</form>
