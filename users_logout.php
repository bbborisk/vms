<?php
/**
 * Louout
 */

logAdd("[logout] User '{$_SESSION['username']}' logged out.");
unset($_SESSION["username"]);
unset($_SESSION['user_id']);
unset($_SESSION['user_type']);
unset($_SESSION['user_full_name']);
session_destroy();
header("Location: index.php");
