<?php
/**
 * Set new password after reset request page
 */

$wrong_password_confirmation = false;

if (!isset($_GET["authorization_code"])) {
    header("Location: index.php");
    die();
} else {
    $authorization_code = clean($_GET["authorization_code"]);
}
$select_user = "
    SELECT id, email, password_reset_at
    FROM users
    WHERE authorization_code = '{$authorization_code}'";
if (!$result = pg_query($select_user)) {
    die("Error executing query." . pg_last_error());
} else {
    if (pg_num_rows($result) == 0) {
        header("Location: index.php");
        die();
    } else {
        $user = pg_fetch_assoc($result);
        $user_id = $user['id'];
        $password_reset_at = $user['password_reset_at'];
        if (time() > strtotime('+1 hour', strtotime($password_reset_at))) {
            $update_user = "
                UPDATE users
                SET
                    password_reset_at = NULL,
                    authorization_code = ''
                WHERE id = '{$user_id}'";
            if (!$result_update = pg_query($update_user)) {
                die("Error executing query." . pg_last_error());
            } else {
                header("Location: index.php");
                die();
            }
        }
    }
}

if (isset($_POST["submit"])) {
    $password_salt = $_secrets['password_salt'];
    $password = clean($_POST["password"]);
    $password_confirmation = clean($_POST["password_confirmation"]);
    $user_id = $_POST["user_id"];

    if ($password != $password_confirmation) {
        $wrong_password_confirmation = true;
    } else {
        $password_hash = md5($password . $password_salt);
        $update_user = "
            UPDATE users
            SET
                password_hash = '{$password_hash}',
                authorization_code = ''
            WHERE id = '{$user_id}'";
        if (!$result = pg_query($update_user)) {
            die("Error executing query." . pg_last_error());
        } else {
            logAdd("[pass_recovery] User with id ='{$user_id}' changed his password after reset request.");
            header("Location: index.php");
            die();
        }
    }
}
?>
<form class="form-signin" role="form" method="POST" action="" onsubmit="return checkLostPassForm()">
    <input class="hidden" type="text" value="<?php echo $user_id; ?>" id="user_id" name="user_id"/>
    <input class="form-control" type="password" required="" placeholder="<?php echo $i18n['login.password_placeholder'] ?>" id="password" name="password"/>
    <input class="form-control" type="password" required="" placeholder="<?php echo $i18n['login.password_confirmation_placeholder'] ?>" id="password_confirmation" name="password_confirmation"/>
    <input class="btn btn-lg btn-primary btn-block" type="submit" id="submit" name="submit" value="<?php echo $i18n['login.set_new_password'] ?>"/>
    <br/>
    <div class="alert alert-warning">
<?php
if ($wrong_password_confirmation) {
    echo $i18n['login.wrong_password_confirmation'];
} else {
    echo $i18n['login.set_new_password'];
}
?>
    </div>
</form>
