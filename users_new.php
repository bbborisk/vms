<?php
/**
 * Add new user page
 */

if (!isset($_SESSION['user_id']) || $_SESSION['user_type'] > 2) {
    header("Location: index.php");
    die();
}

$wrong_username = false;
$wrong_username_confirmation = false;
$wrong_password = false;
$wrong_password_confirmation = false;
$user_created = false;

if (isset($_POST["submit"])) {
    $password_salt = $_secrets['password_salt'];
    $username = clean($_POST["username"]);
    $username_confirmation = clean($_POST["username_confirmation"]);
    $password = clean($_POST["password"]);
    $password_confirmation = clean($_POST["password_confirmation"]);
    $user_type = clean($_POST["user_type"]);
    $full_name = clean($_POST["full_name"]);
    $occupation = clean($_POST["occupation"]);
    $telephone = clean($_POST["telephone"]);
    $password_hash = md5($password . $password_salt);
    $user_id = $_SESSION['user_id'];
    $authorized = (isset($_POST["authorized"])) ? '1' : '0';
    $user_alias = clean($_POST["alias"]);
    $timestamp = date("Y-m-d H:i:s");

    if ($username != $username_confirmation) {
        $wrong_username_confirmation = true;
    } elseif ($password != $password_confirmation) {
        $wrong_password_confirmation = true;
    } else {
        $select_user = "
            SELECT *
            FROM users
            WHERE
                email = '{$username}'";
        if (!empty($user_alias)) {
            $select_user .= "OR alias = '{$user_alias}'";
        }
        if (!$result_select_user = pg_query($select_user)) {
            die("Error executing query." . pg_last_error());
        } else {
            if (pg_num_rows($result_select_user) == 0) {
                $create_user = "
                    INSERT INTO users (type, email, password_hash, full_name, telephone, occupation, authorized, authorization_code, alias, created_at)
                    VALUES ('{$user_type}', '{$username}', '{$password_hash}', '{$full_name}', '{$telephone}', '{$occupation}', '{$authorized}', '', '{$user_alias}', '{$timestamp}');";
                if (!$result_create_user = pg_query($create_user)) {
                    die("Error executing query." . pg_last_error());
                } else {
                    $user_created = true;
                }
            } else {
                $wrong_username = true;
            }
        }
    }
}
?>
<div class="container whitebg">
    <form class="form-other" role="form" method="POST" action="" onsubmit="return checkNewUserForm()">
        <h2><?php echo $i18n['users.add_new']; ?></h2>
        <br/>
        <span class=""><?php echo $i18n['users.type']; ?></span>
        <div class="radio">
            <label for="usertype3"><?php echo $i18n['users.volunteer']; ?></label>
            <input type="radio" name="user_type" id="usertype3" value="3" checked>
        </div>
        <div class="radio">
            <label for="usertype2"><?php echo $i18n['users.demonstrator']; ?></label>
            <input type="radio" name="user_type" id="usertype2" value="2">
        </div>
        <div class="radio">
            <label for="usertype1"><?php echo $i18n['users.administrator']; ?></label>
            <input type="radio" name="user_type" id="usertype1" value="1">
        </div>
        <input class="form-control" type="username" placeholder="<?php echo $i18n['users.alias_placeholder']; ?>" id="alias" name="alias"/>
        <br/>
        <input class="form-control form-control-top" type="username" required="" placeholder="<?php echo $i18n['users.email_placeholder']; ?>" id="username" name="username"/>
        <input class="form-control form-control-bottom" type="username" required="" placeholder="<?php echo $i18n['users.email_confirmation_placeholder']; ?>" id="username_confirmation" name="username_confirmation"/>
        <br/>
        <input class="form-control form-control-top" type="password" required="" placeholder="<?php echo $i18n['users.password_placeholder']; ?>" id="password" name="password"/>
        <input class="form-control form-control-bottom" type="password" required="" placeholder="<?php echo $i18n['users.password_confirmation_placeholder']; ?>" id="password_confirmation" name="password_confirmation"/>
        <br/>
        <input class="form-control form-control-top" type="text" required="" placeholder="<?php echo $i18n['users.full_name_placeholder']; ?>" id="full_name" name="full_name"/>
        <input class="form-control form-control-top form-control-bottom" type="text" required="" placeholder="<?php echo $i18n['users.occupation_placeholder']; ?>" id="occupation" name="occupation"/>
        <input class="form-control form-control-bottom" type="text" required="" placeholder="<?php echo $i18n['users.telephone_placeholder']; ?>" id="telephone" name="telephone"/>
        <div class="checkbox">
            <label for="authorized"><?php echo $i18n['users.authorized']; ?></label>
            <input id="authorized" name="authorized" type="checkbox" checked value="1" >
        </div>
        <input class="btn btn-lg btn-primary btn-block" type="submit" id="submit" name="submit" value="<?php echo $i18n['add']; ?>"/>
        <br/>
        <div class="alert alert-warning">
<?php
if ($wrong_username) {
    echo $i18n['users.wrong_username_message'];
} elseif ($wrong_username_confirmation) {
    echo $i18n['users.wrong_username_confirmation_message'];
} elseif ($wrong_password_confirmation) {
    echo $i18n['users.wrong_password_confirmation_message'];
} elseif ($user_created) {
    echo $i18n['users.created_success'];
} else {
    echo $i18n['users.add_new'];
}
?>
        </div>
    </form>
</div>
