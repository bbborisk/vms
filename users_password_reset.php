<?php
/**
 * Password reset request page
 */

if (!isset($_GET["username"])) {
    header("Location: index.php");
    die();
} else {
    $username = $_GET["username"];
}

$select_user = "
    SELECT id, email
    FROM users
    WHERE
        email = '{$username}' OR
        alias = '{$username}'";
if (!$result_user = pg_query($select_user)) {
    die("Error executing query." . pg_last_error());
} else {
    if (pg_num_rows($result_user) == 0) {
        header("Location: index.php");
        die();
    } else {
        $user = pg_fetch_assoc($result_user);
        $user_id = $user['id'];
        $email = $user['email'];
        $password_salt = $_secrets['password_salt'];
        $password_reset_at = strftime('%F %T', time());
        $authorization_code = md5($password_reset_at . $password_salt);
        $update_user = "
            UPDATE users
            SET
                authorization_code = '{$authorization_code}',
                password_reset_at = '{$password_reset_at}'
            WHERE id = '{$user_id}'";
        if (!$result_update = pg_query($update_user)) {
            die("Error executing query." . pg_last_error());
        } else {
            logAdd("[request_password_reset] User '{$email}' requested a password reset.");
        }
        $send_to = $email;
        $subject = $i18n['password_reset.email_subject'];
        $from = $i18n['password_reset.from_email'];
        $reply_to = $i18n['password_reset.reply_to_email'];
        $headers = "From: " . "<" . $from .">\r\n";
        $headers .= "Reply-To: " . $reply_to . "\r\n";
        $headers .= "Return-path: " . $reply_to;
        $message = $i18n['password_reset.message'] . "\r\n\r\n" .
            $i18n['host_address'] . "index.php?content=users_lost_password&authorization_code=" . $authorization_code;
        mail($send_to, $subject, $message, $headers, "-f $from");
    }
}
?>
<div class="whitebg-full">
    <div class="alert alert-info halfwidth centered">
        <?php echo $i18n['password_reset.password_reset_message']; ?>
    </div>
    <br>
    <div class="halfwidth centered">
        <a class="btn btn-lg btn-primary" href="index.php"><?php echo $i18n['ok']; ?></a>
    </div>
</div>
